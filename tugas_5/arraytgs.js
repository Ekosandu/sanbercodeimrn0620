//Soal Program Pertama

function range (startNum, finishNum){
    var angka =[]
    if (startNum == null || finishNum == null){
        return -1
    } else {
        if (startNum < finishNum){
            for (a = startNum; a<=finishNum; a++){
                angka.push(a)
            }
        } else {
            for (a = startNum; a >= finishNum; a--){
                angka.push(a)
            }
        }
        return angka
    }
}

console.log("Hasil Soal Pertama")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal Progragm Ke-2.
function rangeWithStep (startNum, finishNum, step){
    var jumlah =[]
    if (step == null){
        step = 1
    }
    if (startNum != null && finishNum == null){
        jumlah.push(startNum)
    } 
    else {
        if (startNum < finishNum){
            for (x = startNum; x<=finishNum; x+=step){
                jumlah.push(x)
            }
        } else {
            for (x = startNum; x >= finishNum; x-=step){
                jumlah.push(x)
            }
        }
    }
    return jumlah
}
console.log()
console.log("Hasil Soal Ke-2")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

// Soal Program Ke-3

function sum(startNum, finishNum, step){
    var jumlah = rangeWithStep(startNum, finishNum, step)
    var sum = 0;
    for (x = 0; x < jumlah.length; x++){
        sum += jumlah[x]
    }
    return sum
}
console.log()
console.log("Hasil Soal Ke-3")
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

// Soal Program Ke-4

function dataHandling(input){
    var data = ""
    for (i = 0; i<input.length; i++){
        data += `Nomor ID : ${input[i][0]}\n`
        data += `Nama Lengkap : ${input[i][1]}\n`
        data += `TTL : ${input[i][2]} ${input[i][3]}\n`
        data += `Hobi : ${input[i][4]}\n`
        data += "\n"
    }
    return data
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log()
console.log("Hasil Soal Ke-4")
console.log(dataHandling(input))

// Soal Program Ke-5

function balikKata(ktbalik){
    var kata = "";
    for (i = ktbalik.length-1; i >= 0; i--){
        kata += ktbalik[i]
    }
    return kata
}
console.log()
console.log("Hasil Soal Ke-5")
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
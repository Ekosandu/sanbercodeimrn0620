// Soal 1. Array to Object
function arrayToObject(array){
    
    var biografi = {}
    var object = {}
    var hasil = []

    var now = new Date()
    var thisYear = now.getFullYear()

    for (var a = 0; a < array.length; a++){
        if(array[a][3] == null || array[a][3] > thisYear){
            object = {
                firstName : array[a][0],
                lastName : array[a][1],
                gender : array[a][2],
                age : "Invalid Birth Year"
            }
        } else {
            object = {
                firstName : array[a][0],
                lastName : array[a][1],
                gender : array[a][2],
                age : thisYear - array[a][3]
            }
        }
        biografi[`${array[a][0]} ${array[a][1]}`] = object
    }
    hasil.push(biografi)
    
    return hasil
}
    

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

console.log('Program Array to Object')
console.log(arrayToObject(people))
console.log(arrayToObject(people2))
console.log()

// Soal 2. Shopping Time
function shoppingTime(memberId, money){

    var barangblnja = [
        ["Sepatu Stacattu", 1500000], 
        ["Baju Zoro", 500000], 
        ["Baju H&N", 250000], 
        ["Sweater Uniklooh", 175000], 
        ["Casing Handphone", 50000]
    ]

    var duit = money
    var barangBelanja = []

    if (memberId == null || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    for (var a = 0; a < barangblnja.length; a++){
        if (barangblnja[a][1] <= duit){
            duit -= barangblnja[a][1]
            barangBelanja.push(barangblnja[a][0])
        } else {
            continue
        }
    }
    var barangblnja = {
        memberId: memberId,
        duit: money,
        listPurchased: barangBelanja,
        changeMoney: duit
    }
    return barangblnja
}

console.log('Program ShoppingTime')
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 
console.log()

//Program Naik Angkot
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var angkot = []
    for (var a = 0; a < listPenumpang.length; a++){
        var tarif = (rute.indexOf(listPenumpang[a][2]) - rute.indexOf(listPenumpang[a][1])) * 2000
        penumpang = {
            penumpang: listPenumpang[a][0],
            naikDari: listPenumpang[a][1],
            tujuan: listPenumpang[a][2],
            bayar: tarif
        }
        angkot.push(penumpang)
    }
    return angkot
}

console.log('Program angkot')
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); 

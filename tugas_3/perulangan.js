//Soal loping menggunakan While

console.log('LOOPING PERTAMA')

var angka = 0;

while (angka < 20 ){
    angka += 2;
    console.log(angka + ' - I love coding');
}

console.log()

console.log('LOOPING KEDUA')

var angka = 22;

while (angka > 2 ){
    angka -= 2;
    console.log(angka + ' - I love coding');
}

//Soal looping menggunakan For

console.log()

var angka = 1;

for ( angka = 1; angka <= 20; angka++){
    if ( angka % 2 != 0 &&  angka % 3 == 0){
        console.log(angka+ ' - I Love Coding ')
    }
    else if (angka % 2 != 0){
        console.log(angka+ ' - Santai')
    }
    else if (angka % 2 == 0){
        console.log( angka+ ' - Berkualitas')
    }
}

//Soal membuat kotak

console.log()

var kotak = ""
var panjang = 8;
var lebar = 4;

for (a = 0; a<lebar; a++){
    for (k = 0; k<panjang; k++){
        kotak +="#"
    }
    kotak += "\n"
}
console.log(kotak)


//Soal membuat tangga

var kotak = ""
var alas = 7
var tinggi = 7


for (a = 0; a<tinggi; a++){
    for(k = 0; k<=a; k++){
        kotak+= "#"
    }
    kotak += "\n"
}
console.log(kotak)

//Soal membuat papan catur

var sisi = 8
var jarak = ""
for (a = 0; a<sisi; a++){
    for (s = 0; s<sisi; s++){
        if (a % 2 != 0){
            if (s % 2 != 0){
               jarak +=" "
            }
            else {
                jarak +="#"
            }
        } else {
            if (s % 2 == 0){
                jarak +=" "
            }
            else {
                jarak +="#"
            }
        }
    }
    jarak += "\n"
}
console.log(jarak)


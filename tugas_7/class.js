//Program Class Animal - Release 0

class Animal {

	constructor(name){
	  this.name = name;
	  this.kaki = 4;
	  this.cold_blooded = false
    }
    get legs(){
        return this.kaki
    }    
}
var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
console.log()

//Program Class Animal - Release 1

class Ape extends Animal {
	constructor (name){
		super(name);
		this.kaki = 2;
		this.yel = "Auoo";
	}
	get legs(){
        return this.kaki
    }    
	yell(){
		console.log(this.yel)
	}
}

class Frog extends Animal {
	constructor (name){
		super(name);
		this.loncat = "hop hop";
	}
	jump(){
		console.log(this.loncat)
	}

}

var sungokong = new Ape("kera sakti");
sungokong.yell();

var kodok = new Frog("buduk");
kodok.jump();
console.log()

//Program Function to Class

class Clock {
    constructor({ template }){
        this.template = template
        this.timer
    }

    render(){
        var date = new Date()

        var hours = date.getHours()
        if (hours < 10) hours = '0' + hours

        var mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins

        var secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)

        console.log(output)
    }
    
    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.render
        this.timer = setInterval(this.render.bind(this), 1000)
    }
}


var clock = new Clock({template: 'h:m:s'})
clock.start()




function bandingkan(num1, num2) {
  // code di sini
  if (num1 < 0 || num2 < 0){
    return -1
  } else if (num1 == num2) {
    return -1
  } else if (num1 > num2) {
    return num1
  } else if (num2 > num1) {
    return num2
  } else if (num1 != null && num2 == null) {
    return num1
  }
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


//Program Mengubah Fungsi jadi Fungsi Arrow

const golden = () => {
	console.log("this is golden!!")	
}
golden()

// Program Menyederhanakan Object Literal di ES6
const newFunction = function literal(firstName, lastName){
	return {
		 firstName,
		 lastName,
		 fullName: function(){
			console.log(firstName + "" + lastName)
			return
		}
	}
}

//Driver Code
console.log()
newFunction("William", "Imoh").fullName()

// Program Destructuring

const newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!"
}

console.log()
const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation, spell)

// Program Array Spreading

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

let combined = [... west,... east]

//Driver Code
console.log()
console.log(combined)

const planet = "earth"
const view = "glass"
const before = 'Lorem '+ `${view}` + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,'+ ` ${planet}` + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 

// Driver Code
console.log()

console.log(before) 
//menampilkan jadi satu kalimat

var word = 'Javascript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it';

console.log(word,second,third,fourth,fifth,sixth,seventh)

//mengurai kalimat

var sentences = "I am going to be React Native Developer";
var exampleFirstWord = sentences[0];
var exampleSecondWord = sentences[2] + sentences[3];
var exampleThirdWord = sentences[5] + sentences[6] + sentences[7] + sentences[8] + sentences[9];
var exampleFourthWord = sentences[11] + sentences[12];
var exampleFifthWord = sentences[14] + sentences[15];
var exampleSixthWord = sentences[17] + sentences[18] + sentences[19] + sentences[20] + sentences[21];
var exampleSeventhWord = sentences[23] + sentences[24] + sentences[25] + sentences[26] + sentences[27] + sentences[28];
var exampleEighthWord = sentences[30] + sentences[31] + sentences[32] + sentences[33] + sentences[34] + sentences[35] + sentences[36] + sentences[37] + sentences[38];

console.log('First word : ' +exampleFirstWord);
console.log('Second word : ' +exampleSecondWord);
console.log('Third word : ' +exampleThirdWord);
console.log('Fourth word : ' +exampleFourthWord);
console.log('Fifth word : ' +exampleFifthWord);
console.log('Sixth word : ' +exampleSixthWord);
console.log('Seventh word : ' +exampleSeventhWord);
console.log('Eighth word : ' +exampleEighthWord);

//mengurai kalimat ke-2
var sentence2 = 'Wow Javascript is so cool';

var exampleFirstWord2 = sentence2.substring(0,3);
var exampleSecondWord2 = sentence2.substring(4,14);
var exampleThirdWord2 = sentence2.substring(15,17);
var exampleFourthWord2 = sentence2.substring(18,20);
var exampleFifthWord2 = sentence2.substring(21,25);

console.log('First word : ' +exampleFirstWord2);
console.log('Second word : ' +exampleSecondWord2);
console.log('Third word : ' +exampleThirdWord2);
console.log('Fourth word : ' +exampleFourthWord2);
console.log('Fifth word : ' +exampleFifthWord2);


//mengurai kalimat dgn menghitung karakter
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var exampleSecondWord3 = sentence3.substring(4,14);
var exampleThirdWord3 = sentence3.substring(15,17);
var exampleFourthWord3 = sentence3.substring(18,20);
var exampleFifthWord3 = sentence3.substring(21,25);

var FirstWordLength = exampleFirstWord3.length;
var SecondWordLength = exampleSecondWord3.length;
var ThirdWordLength = exampleThirdWord3.length;
var FourthWordLength = exampleFourthWord3.length;
var FifthWordLength = exampleFifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + FirstWordLength);    
console.log('Second Word: ' + exampleSecondWord3 + ', with length: ' + SecondWordLength);   
console.log('Third Word: ' + exampleThirdWord3 + ', with length: ' + ThirdWordLength);  
console.log('Fourth Word: ' + exampleFourthWord3 + ', with length: ' + FourthWordLength); 
console.log('Fifth Word: ' + exampleFifthWord3 + ', with length: ' + FifthWordLength); 
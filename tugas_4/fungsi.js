//Program Fungsi Teriak

console.log("Program Fungsi Teriak")

function teriak(){
  var teriak = "Halo Sanbers"
  return teriak
}

console.log(teriak())

console.log()

//Program Fungsi Kalikan
console.log("Program Fungsi Kalikan")

function kalikan(){
  return num1 * num2
}

var num1 = 12
var num2 = 4
var hasilkali = kalikan(num1,num2)
console.log(hasilkali)

console.log()

//Program Fungsi Introduce
console.log("Program Fungsi Introduce")

function introduce(name, age, address, hobby){
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
